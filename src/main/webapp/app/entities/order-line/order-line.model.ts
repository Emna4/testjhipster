import { IOrder } from 'app/entities/order/order.model';
import { IProduct } from 'app/entities/product/product.model';

export interface IOrderLine {
  id?: number;
  quantity?: number | null;
  price?: number | null;
  order?: IOrder | null;
  product?: IProduct | null;
}

export class OrderLine implements IOrderLine {
  constructor(
    public id?: number,
    public quantity?: number | null,
    public price?: number | null,
    public order?: IOrder | null,
    public product?: IProduct | null
  ) {}
}

export function getOrderLineIdentifier(orderLine: IOrderLine): number | undefined {
  return orderLine.id;
}
